using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PayQuick.Client.Abstractions;
using PayQuick.Contracts;

namespace PayQuick.Client
{
    public class PayQuickClient : IPayQuickClient
    {
        private readonly Uri _baseUrl;
        private readonly JsonSerializerSettings _serializerSettings;

        public PayQuickClient(IOptions<PayQuickClientSettings> settings)
        {
            _baseUrl = new Uri(settings.Value.Url);
            _serializerSettings = new JsonSerializerSettings();
            _serializerSettings.DateParseHandling = DateParseHandling.DateTime;
        }
        
        public async Task<CreatePaymentResponse> CreatePayment(CreatePaymentRequest request)
        {
            return await SendRequest<CreatePaymentRequest, CreatePaymentResponse>(request, "create");
        }

        public async Task<GetTransactionsResponse> GetTransactions(GetTransactionsRequest request)
        {
            return await SendRequest<GetTransactionsRequest, GetTransactionsResponse>(request, "transactions");
        }

        private async Task<TResponse> SendRequest<TRequest, TResponse>(TRequest request, string path) where TResponse: new()
        {
            var client = new HttpClient();
            
            var message = new HttpRequestMessage();
            message.RequestUri = new Uri(_baseUrl, path);
            message.Method = HttpMethod.Post;
            message.Content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.SendAsync(message);

            return JsonConvert.DeserializeObject<TResponse>(await response.Content.ReadAsStringAsync());
        }
    }
}