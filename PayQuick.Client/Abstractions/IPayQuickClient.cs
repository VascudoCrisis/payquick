using System.Threading.Tasks;
using PayQuick.Contracts;

namespace PayQuick.Client.Abstractions
{
    public interface IPayQuickClient
    {
        Task<CreatePaymentResponse> CreatePayment(CreatePaymentRequest request);
        Task<GetTransactionsResponse> GetTransactions(GetTransactionsRequest request);
    }
}