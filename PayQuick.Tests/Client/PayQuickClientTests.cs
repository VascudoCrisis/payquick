using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayQuick.Client;
using PayQuick.Contracts;

namespace PayQuick.Tests.Client
{
    
    [TestClass]
    public class PayQuickClientTests
    {
        [TestMethod]
        public async Task CreatePayment()
        {
            var client = new PayQuickClient(new OptionsWrapper<PayQuickClientSettings>(new PayQuickClientSettings
            {
                Url = "http://localhost:5000/"
            }));

            var createPaymentRequest = new CreatePaymentRequest
            {
                AccountNumber = 0,
                Amount = 34.56m,
                FirstName = "John",
                LastName = "Sample",
                Email = "email@domain.com",
                Phone = "2125551212",
                Address = "12 Sample Street",
                AdditionalAddress = "apt. 23B",
                City = "New York",
                State = "NY",
                ZipCode = 10000,
                Country = "USA",
                DateOfBirth = new DateTime(2001, 2, 1),
                UserIp = "100.100.100.199",
                PaymentSystem = PaymentSystem.Mastercard,
                Cardholder = "John D Sample",
                CardNumber = "4111111111111111",
                CVV = "123",
                ExpiryMonth = 11,
                ExpiryYear = 2020,
                IdempotentKey = Guid.NewGuid()
            };

            var result = await client.CreatePayment(createPaymentRequest);
            
            Assert.AreEqual("APPROVED", result.Status);
            Assert.AreEqual(34.56m, result.Amount);
        }

        [TestMethod]
        public async Task GetTransactions()
        {
            var client = new PayQuickClient(new OptionsWrapper<PayQuickClientSettings>(new PayQuickClientSettings
            {
                Url = "http://localhost:5000/"
            }));

            var getTransactionsRequest = new GetTransactionsRequest
            {
                StartDate = new DateTime(2019, 4, 1),
                EndDate = new DateTime(2019, 7, 31),
                Status = TransactionStatus.Approved
            };

            var result = await client.GetTransactions(getTransactionsRequest);

            Assert.AreEqual("API Test Successful", result.Status);
        }
    }
}