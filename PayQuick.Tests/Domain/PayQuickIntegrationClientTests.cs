using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayQuick.Domain;
using PayQuick.Domain.Models;

namespace PayQuick.Tests.Domain
{
    [TestClass]
    
    public class PayQuickIntegrationClientTests
    {
        private IHttpClientFactory _factory;
        private PayQiuckIntegrationClientSettings _settings;

        [TestInitialize]
        public void Init()
        {
            IServiceCollection services = new ServiceCollection(); // [1]

            _factory = services.AddHttpClient()
                .BuildServiceProvider()
                .GetRequiredService<IHttpClientFactory>();
            
            _settings = new PayQiuckIntegrationClientSettings
            {
                Url = "https://payquick.tk/portal/api/",
                ApiKey = "pq0000000000000000000"
            };
        }
        
        [TestMethod]
        public async Task CreatePayment()
        {
            var client = new PayQuickIntegrationClient(_factory, new OptionsWrapper<PayQiuckIntegrationClientSettings>(_settings));
            
            var createPaymentModel = new CreatePaymentModel
            {
                AccountNumber = 0,
                Amount = 34.56m,
                FirstName = "John",
                LastName = "Sample",
                Email = "email@domain.com",
                Phone = "2125551212",
                Address = "12 Sample Street",
                AdditionalAddress = "apt. 23B",
                City = "New York",
                State = "NY",
                ZipCode = 10000,
                Country = "USA",
                DateOfBirth = new DateTime(2001, 2, 1),
                UserIp = "100.100.100.199",
                PaymentSystem = PaymentSystem.Mastercard,
                Cardholder = "John D Sample",
                CardNumber = "4111111111111111",
                CVV = "123",
                ExpiryMonth = 11,
                ExpiryYear = 2020
            };
            
            var result = await client.CreatePayment(createPaymentModel);
            
            Assert.AreEqual("APPROVED",result.Status);
        }
        
        [TestMethod]
        public async Task GetTransactions()
        {
            var client = new PayQuickIntegrationClient(_factory, new OptionsWrapper<PayQiuckIntegrationClientSettings>(_settings));
            
            var getTransactionsModel = new GetTransactionsModel
            {
                StartDate = new DateTime(2019, 4, 1),
                EndDate = new DateTime(2019, 7, 31),
                Status = TransactionStatus.Approved
            };
            
            var result = await client.GetTransactions(getTransactionsModel);
            
            Assert.AreEqual("API Test Successful", result.Status);
        }
    }
}