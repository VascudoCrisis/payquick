﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PayQuick.Domain;

namespace PayQuick.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddLogging()
                .AddHttpClient()
                .RegisterDomain(_configuration)
                .AddMvcCore()
                .AddJsonFormatters(o =>
                {
                    o.Converters.Add(new StringEnumConverter());
                    o.FloatParseHandling = FloatParseHandling.Decimal;
                    o.NullValueHandling = NullValueHandling.Ignore;
                });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMvcWithDefaultRoute();
        }
    }
}