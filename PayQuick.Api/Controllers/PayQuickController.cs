using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PayQuick.Api.Mappers;
using PayQuick.Contracts;
using PayQuick.Domain.Abstractions;

namespace PayQuick.Api.Controllers
{
    [Route("")]
    public class PayQuickController : Controller
    {
        private readonly IQuickPaymentManager _manager;

        public PayQuickController(IQuickPaymentManager manager)
        {
            _manager = manager;
        }
        
        [HttpPost("create")]
        public async Task<ActionResult<CreatePaymentResponse>> CreatePayment([FromBody] CreatePaymentRequest request)
        {
            return Ok(Mapper.Map(await _manager.CreatePayment(Mapper.Map(request))));
        }

        [HttpPost("transactions")]
        public async Task<ActionResult<GetTransactionsResponse>> GetTransactions(GetTransactionsRequest request)
        {
            return Ok(Mapper.Map(await _manager.GetTransactions(Mapper.Map(request))));
        }
    }
}