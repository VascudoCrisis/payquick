using System;
using System.Globalization;
using PayQuick.Contracts;
using PayQuick.Domain.Models;

namespace PayQuick.Api.Mappers
{
    public static class Mapper
    {
        public static CreatePaymentModel Map(CreatePaymentRequest request)
        {
            return new CreatePaymentModel
            {
                AccountNumber = request.AccountNumber,
                Amount = request.Amount,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Address = request.Address,
                AdditionalAddress = request.AdditionalAddress,
                City = request.City,
                Country = request.Country,
                Email = request.Email,
                Phone = request.Phone,
                State = request.State,
                ZipCode = request.ZipCode,
                DateOfBirth = request.DateOfBirth,
                UserIp = request.UserIp,
                PaymentSystem = (Domain.Models.PaymentSystem)request.PaymentSystem,
                Cardholder = request.Cardholder,
                CardNumber = request.CardNumber,
                CVV = request.CVV,
                ExpiryMonth = request.ExpiryMonth,
                ExpiryYear = request.ExpiryYear,
                IdempotentKey = request.IdempotentKey
            };
        }

        public static GetTransactionsModel Map(GetTransactionsRequest r)
        {
            return new GetTransactionsModel
            {
                StartDate = r.StartDate,
                EndDate = r.EndDate,
                Status =  (Domain.Models.TransactionStatus)r.Status
            };
        }

        public static CreatePaymentResponse Map(CreatePaymentResultModel response)
        {
            var res = new CreatePaymentResponse
            {
                Status = response.Status,
                Merchant = response.Merchant,
                Error = response.Error
            };

            if (!string.IsNullOrWhiteSpace(response.Datetime))
            {
                res.Datetime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                    .AddSeconds(long
                        .Parse(response.Datetime)).ToLocalTime();
            }

            if (decimal.TryParse(response.Amount, out var amount))
            {
                res.Amount = amount;
            }

            return res;
        }

        public static GetTransactionsResponse Map(GetTransactionsResultModel response)
        {
            return new GetTransactionsResponse
            {
                Status = response.Status,
                Error = response.Error
            };
        }
    }
}