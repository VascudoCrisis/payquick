using System.Threading.Tasks;
using PayQuick.Domain.Models;

namespace PayQuick.Domain.Abstractions
{
    public interface IPayQuickIntegrationClient
    {
        Task<CreatePaymentResultModel> CreatePayment(CreatePaymentModel createPaymentModel);
        Task<GetTransactionsResultModel> GetTransactions(GetTransactionsModel getTransactionsModel);
    }
}