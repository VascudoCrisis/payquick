using System.Threading.Tasks;
using PayQuick.Domain.Models;

namespace PayQuick.Domain.Abstractions
{
    public interface IQuickPaymentManager
    {
        Task<CreatePaymentResultModel> CreatePayment(CreatePaymentModel model);
        Task<GetTransactionsResultModel> GetTransactions(GetTransactionsModel model);
    }
}