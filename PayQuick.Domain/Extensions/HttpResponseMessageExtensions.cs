using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PayQuick.Domain.Extensions
{
    public static class HttpResponseMessageExtensions
    {
        public static async Task<T> GetFlatModel<T>(this Task<HttpResponseMessage> response) where T : new()
        {
            var result = await (await response).Content.ReadAsStringAsync();

            var resultProperties = GetTuples(result).ToDictionary(e => e.Item1, e => e.Item2);

            var model = new T();
            
            foreach (var property in model.GetType().GetProperties())
            {
                if (resultProperties.TryGetValue(property.Name, out var value))
                {
                    property.SetValue(model, value);
                }
            }

            return model;
        }

        private static IEnumerable<(string, string)> GetTuples(string input)
        {
            var matches = Regex.Matches(input, @"(""(?<key>(\w+))"":[""]{0,1}(?<value>([\w\s\.]+))[""]{0,1})");
            
            foreach (Match match in matches)
            {
                yield return (match.Groups["key"].Value, match.Groups["value"].Value);
            }
        }
    }
}