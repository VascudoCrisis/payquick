using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PayQuick.Domain.Abstractions;

namespace PayQuick.Domain
{
    public static class ServicesCollectionExtensions
    {
        public static IServiceCollection RegisterDomain(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .AddOptions()
                .Configure<PayQiuckIntegrationClientSettings>(configuration.GetSection("PayQuickSettings"))
                .AddTransient<IPayQuickIntegrationClient, PayQuickIntegrationClient>()
                .AddTransient<IQuickPaymentManager, QuickPaymentManager>();
        }
    }
}