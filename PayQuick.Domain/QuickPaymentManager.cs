using System.Threading.Tasks;
using PayQuick.Domain.Abstractions;
using PayQuick.Domain.Models;

namespace PayQuick.Domain
{
    internal class QuickPaymentManager : IQuickPaymentManager
    {
        private readonly IPayQuickIntegrationClient _client;

        public QuickPaymentManager(IPayQuickIntegrationClient client)
        {
            _client = client;
        }
        public async Task<CreatePaymentResultModel> CreatePayment(CreatePaymentModel model)
        {
            //Save and check IdempotentKey here
            
            return await _client.CreatePayment(model);
        }

        public async Task<GetTransactionsResultModel> GetTransactions(GetTransactionsModel model)
        {
            return await _client.GetTransactions(model);
        }
    }
}