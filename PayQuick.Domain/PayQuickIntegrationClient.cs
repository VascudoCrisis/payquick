using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using PayQuick.Domain.Abstractions;
using PayQuick.Domain.Extensions;
using PayQuick.Domain.Models;

namespace PayQuick.Domain
{
    public class PayQuickIntegrationClient : IPayQuickIntegrationClient
    {
        private readonly IHttpClientFactory _factory;
        private readonly string _apiKey;
        private readonly Uri _baseUrl;

        public PayQuickIntegrationClient(IHttpClientFactory factory, IOptions<PayQiuckIntegrationClientSettings> settings)
        {
            _factory = factory;
            _baseUrl = new Uri(settings.Value.Url);
            _apiKey = settings.Value.ApiKey;
        }
        
        public async Task<CreatePaymentResultModel> CreatePayment(CreatePaymentModel createPaymentModel)
        {
            var client = _factory.CreateClient();

            createPaymentModel.ApiKey = _apiKey;

            return await client.PostAsync(new Uri(_baseUrl, "processPayment"),
                new FormUrlEncodedContent(createPaymentModel.ToPayQuickParameters()))
                .GetFlatModel<CreatePaymentResultModel>();
        }
        
        public async Task<GetTransactionsResultModel> GetTransactions(GetTransactionsModel getTransactionsModel)
        {
            var client = _factory.CreateClient();

            getTransactionsModel.ApiKey = _apiKey;
            
            return await client.PostAsync(new Uri(_baseUrl, "getTransactions"),
                new FormUrlEncodedContent(getTransactionsModel.ToPayQuickParameters()))
                .GetFlatModel<GetTransactionsResultModel>();
        }
    }
}