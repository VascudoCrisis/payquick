namespace PayQuick.Domain.Models
{
    public class CreatePaymentResultModel
    {
        public string Status { get; set; }
        public string Datetime { get; set; }
        public string Merchant { get; set; }
        public string Amount { get; set; }
        public string Error { get; set; }
    }
}