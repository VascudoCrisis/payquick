namespace PayQuick.Domain.Models
{
    public class GetTransactionsResultModel
    {
        public string Status { get; set; }
        public string Error { get; set; }
    }
}