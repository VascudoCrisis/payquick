using System;
using System.Collections.Generic;

namespace PayQuick.Domain.Models
{
    public class CreatePaymentModel : PayQuickModelBase
    {
        public long AccountNumber { get; set; }
        public decimal Amount { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string AdditionalAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int ZipCode { get; set; }
        public string Country { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string UserIp { get; set; }
        public PaymentSystem PaymentSystem { get; set; }
        public string Cardholder { get; set; }
        public string CardNumber { get; set; }
        public string CVV { get; set; }
        public int ExpiryMonth { get; set; }
        public int ExpiryYear { get; set; }
        public Guid IdempotentKey { get; set; }
        
        public override Dictionary<string, string> ToPayQuickParameters()
        {
            var parameters = new Dictionary<string, string>();
            
            parameters.Add("apikey", ApiKey);
            parameters.Add("account", AccountNumber.ToString());
            parameters.Add("amount", Amount.ToString("F2"));
            parameters.Add("firstName", FirstName);
            parameters.Add("lastName", LastName);
            parameters.Add("email", Email);
            parameters.Add("phone", Phone);
            parameters.Add("address", Address);
            parameters.Add("address2", AdditionalAddress);
            parameters.Add("city", City);
            parameters.Add("state", State);
            parameters.Add("zipCode", ZipCode.ToString());
            parameters.Add("country", Country);
            parameters.Add("DOB", DateOfBirth.ToString("MMddyyyy"));
            parameters.Add("UIP", UserIp);
            parameters.Add("payBy", PaymentSystem.ToString().ToLower());
            parameters.Add("cardName", Cardholder);
            parameters.Add("cardNo", CardNumber);
            parameters.Add("CVV", CVV);
            parameters.Add("expiryMonth", ExpiryMonth.ToString());
            parameters.Add("expiryYear", ExpiryYear.ToString());

            return parameters;
        }
    }

    public enum PaymentSystem
    {
        Mastercard,
        Visa,
        AmericanExpress
    }
}