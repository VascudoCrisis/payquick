using System;
using System.Collections.Generic;

namespace PayQuick.Domain.Models
{
    public class GetTransactionsModel : PayQuickModelBase
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public TransactionStatus Status { get; set; }
        
        public override Dictionary<string, string> ToPayQuickParameters()
        {
            var parameters = new Dictionary<string, string>();
            
            parameters.Add("apikey", ApiKey);
            parameters.Add("startDate", StartDate.ToString("MMddyyyy"));
            parameters.Add("endDate", EndDate.ToString("MMddyyyy"));
            parameters.Add("status", Status.ToString().ToLower());

            return parameters;
        }
    }

    public enum TransactionStatus
    {
        Approved,
        Declined
    }
}