using System.Collections.Generic;

namespace PayQuick.Domain.Models
{
    public abstract class PayQuickModelBase
    {
        internal string ApiKey { get; set; }

        public abstract Dictionary<string, string> ToPayQuickParameters();
    }
}