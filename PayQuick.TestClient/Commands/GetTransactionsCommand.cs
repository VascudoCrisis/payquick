using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PayQuick.Client;
using PayQuick.Contracts;

namespace PayQuick.TestClient.Commands
{
    public class GetTransactionsCommand : ConsoleCommandBase
    {
        public GetTransactionsCommand() : base(ConsoleKey.G)
        {
        }

        public async override Task Execute()
        {
            Console.Clear();

            var client = new PayQuickClient(new OptionsWrapper<PayQuickClientSettings>(new PayQuickClientSettings
            {
                Url = "http://localhost:5000/"
            }));

            var getTransactionsRequest = new GetTransactionsRequest
            {
                StartDate = new DateTime(2019, 4, 1),
                EndDate = new DateTime(2019, 7, 31),
                Status = TransactionStatus.Approved
            };

            var result = await client.GetTransactions(getTransactionsRequest);
            
            Console.WriteLine(JsonConvert.SerializeObject(result));
        }

        public override string ToString()
        {
            return "[G]et transactions";
        }
    }
}