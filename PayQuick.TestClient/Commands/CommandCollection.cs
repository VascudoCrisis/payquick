using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PayQuick.TestClient.Commands
{
    public sealed class CommandCollection : ConsoleCommandBase
    {
        private Dictionary<ConsoleKey, ConsoleCommandBase> _commands;
        
        public CommandCollection(ConsoleKey key) : base(key)
        {
            _commands = new Dictionary<ConsoleKey, ConsoleCommandBase>();
        }

        public CommandCollection Add( ConsoleCommandBase command)
        {
            _commands.Add(command.Key, command);

            return this;
        }
        
        public async override Task Execute()
        {
            while (true)
            {
                Console.WriteLine(ToString());
                var key = Console.ReadKey().Key;

                if (_commands.TryGetValue(key, out var command))
                {
                    await command;
                    
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
                
                Console.Clear();
            }
        }

        public override string ToString()
        {
            return _commands.Aggregate(new StringBuilder().AppendLine("Commands:"),
                (sb, cmd) => sb.AppendLine(cmd.Value.ToString())).ToString();
        }
    }
}