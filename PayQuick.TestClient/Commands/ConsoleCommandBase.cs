using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace PayQuick.TestClient.Commands
{
    public abstract class ConsoleCommandBase
    {
        public ConsoleKey Key { get; }
        
        protected ConsoleCommandBase(ConsoleKey key)
        {
            Key = key;
        }

        public TaskAwaiter GetAwaiter()
        {
            return Execute().GetAwaiter();
        }

        public abstract Task Execute();
        public abstract string ToString();
    }
}