using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PayQuick.Client;
using PayQuick.Contracts;

namespace PayQuick.TestClient.Commands
{
    public class CreatePaymentCommand : ConsoleCommandBase
    {
        public CreatePaymentCommand() : base(ConsoleKey.C)
        {
        }

        public async override Task Execute()
        {
            Console.Clear();
            
            var client = new PayQuickClient(new OptionsWrapper<PayQuickClientSettings>(new PayQuickClientSettings
            {
                Url = "http://localhost:5000/"
            }));

            var createPaymentRequest = new CreatePaymentRequest
            {
                AccountNumber = 0,
                Amount = 34.56m,
                FirstName = "John",
                LastName = "Sample",
                Email = "email@domain.com",
                Phone = "2125551212",
                Address = "12 Sample Street",
                AdditionalAddress = "apt. 23B",
                City = "New York",
                State = "NY",
                ZipCode = 10000,
                Country = "USA",
                DateOfBirth = new DateTime(2001, 2, 1),
                UserIp = "100.100.100.199",
                PaymentSystem = PaymentSystem.Mastercard,
                Cardholder = "John D Sample",
                CardNumber = "4111111111111111",
                CVV = "123",
                ExpiryMonth = 11,
                ExpiryYear = 2020,
                IdempotentKey = Guid.NewGuid()
            };

            var result = await client.CreatePayment(createPaymentRequest);
            
            Console.WriteLine(JsonConvert.SerializeObject(result));
        }

        public override string ToString()
        {
            return "[C]reate new payment";
        }
    }
}