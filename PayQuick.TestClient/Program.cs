﻿using System;
using System.Threading.Tasks;
using PayQuick.TestClient.Commands;

namespace PayQuick.TestClient
{
    class Program
    {
        static void Main(string[] args) => MainAsync(args).GetAwaiter().GetResult();

        static async Task MainAsync(string[] args)
        {
            var coll = new CommandCollection(ConsoleKey.R)
                .Add(new CreatePaymentCommand())
                .Add(new GetTransactionsCommand());

            await coll;
        }
    }
}