using System;
using Newtonsoft.Json;

namespace PayQuick.Contracts
{
    public class CreatePaymentRequest
    {
        public long AccountNumber { get; set; }
        
        [JsonRequired]
        public decimal Amount { get; set; }
        
        [JsonRequired]
        public string FirstName { get; set; }
        
        [JsonRequired]
        public string LastName { get; set; }
        
        [JsonRequired]
        public string Email { get; set; }
        
        [JsonRequired]
        public string Phone { get; set; }
        
        [JsonRequired]
        public string Address { get; set; }
        
        public string AdditionalAddress { get; set; }
        
        [JsonRequired]
        public string City { get; set; }
        
        [JsonRequired]
        public string State { get; set; }
        
        [JsonRequired]
        public int ZipCode { get; set; }
        
        [JsonRequired]
        public string Country { get; set; }
        
        [JsonRequired]
        public DateTime DateOfBirth { get; set; }
        
        [JsonRequired]
        public string UserIp { get; set; }
        
        [JsonRequired]
        public PaymentSystem PaymentSystem { get; set; }
        
        [JsonRequired]
        public string Cardholder { get; set; }
        
        [JsonRequired]
        public string CardNumber { get; set; }
        
        [JsonRequired]
        public string CVV { get; set; }
        
        [JsonRequired]
        public int ExpiryMonth { get; set; }
        
        [JsonRequired]
        public int ExpiryYear { get; set; }
        
        [JsonRequired]
        public Guid IdempotentKey { get; set; }
    }

    public enum PaymentSystem
    {
        Mastercard,
        Visa,
        AmericanExpress
    }
}