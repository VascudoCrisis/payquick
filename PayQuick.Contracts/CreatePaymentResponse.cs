using System;

namespace PayQuick.Contracts
{
    public class CreatePaymentResponse
    {
        public string Status { get; set; }
        public DateTime? Datetime { get; set; }
        public string Merchant { get; set; }
        public decimal? Amount { get; set; }
        public string Error { get; set; }
    }
}