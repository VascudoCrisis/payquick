namespace PayQuick.Contracts
{
    public class GetTransactionsResponse
    {
        public string Status { get; set; }
        public string Error { get; set; }
    }
}